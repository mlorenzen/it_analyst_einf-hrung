package aufgaben;

public class aufgabe_10 {

	public static void main(String[] args) {
		
		int[] array = new int[1000];
		int counter = 0;
		
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (100 * Math.random());
		}
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 42) {
				counter++;
			}
		}
		
		System.out.println("42 ist " + counter + "-mal im Array enthalten.");
		

	}

}
