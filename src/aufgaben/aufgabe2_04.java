package aufgaben;
import java.util.Scanner;

public class aufgabe2_04 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		sc.close();
		
		int i = str.length() -1;
		StringBuilder reverse = new StringBuilder();
		
		while (i >= 0){
			reverse.append(str.charAt(i));
			i--;
		}
		
		System.out.println(reverse);
		

	}

}
