package aufgaben;

public class aufgabe_9 {

	public static void main(String[] args) {
		
		double kapital = 10000.0;
		double zins = 0.021;
		int jahr = 0;
		int[] ergebnis = new int[4];
		double[] ziel = new double[] {15000.0, 20000.0, 25000.0, 30000.0};
		int zielnr = 0;
		
		while (ergebnis[3] == 0) {
			
			jahr++;
			kapital = kapital * (1.0 + zins);
			// System.out.println("Jahr" + jahr + "Kapital " + kapital);
			if (kapital >= ziel[zielnr]) {
				ergebnis[zielnr] = jahr;
				zielnr++;
			}
			
		}
		
		for (int j = 0; j < ergebnis.length; j++) {
			System.out.println(ziel[j] + " sind nach " + ergebnis[j] + " Jahren erreicht.");
		}

	}

}
