package aufgaben;
import java.util.Scanner;

public class aufgabe2_05 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		sc.close();
		
		StringBuilder rueckwaerts = new StringBuilder(str).reverse();
		
		if (str.toLowerCase().contentEquals(rueckwaerts.toString().toLowerCase())){
			System.out.println("Das Wort ist ein Palindrom");
		} else {
			System.out.println("Das Wort ist kein Palindrom");
			
		}
		

	}

}
