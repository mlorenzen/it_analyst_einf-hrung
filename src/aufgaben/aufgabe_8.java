package aufgaben;

public class aufgabe_8 {

	public static void main(String[] args) {

		int counter = 0;

		for (int i = 900; i <= 1000; i++) {

			if (i % 19 == 0) {

				counter++;
			}
		}
		
		System.out.println("In den Zahlen von 900 bis 1000 sind " + counter + " Zahlen durch 19 teilbar.");

	}

}
